<?php
/**
 * Fully safe counter. We open file and lock it, then reading and writing. Our counter have no moments without locks
 * until we will call close().
 */
namespace Brocoder\FileSystem\Examples;

require_once __DIR__ . '/../vendor/autoload.php';

use Brocoder\FileSystem\FileLocked;

// opening the counter with lock
$fl = new FileLocked( __DIR__ . '/counter_safe.tmp' );
// read counter
$counter = ( int ) $fl->readAll();
// writing hit
$fl->rewrite( ++$counter );
echo "Current safe counter value: {$fl->readAll()}";
// release counter
$fl->close();