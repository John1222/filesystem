<?php
/**
 * Standard counter without locks. Not good idea if you want safe counter.
 */
namespace Brocoder\FileSystem\Examples;

require_once __DIR__ . '/../vendor/autoload.php';

use Brocoder\FileSystem\File;

// opening the counter without lock
$fl = new File( __DIR__ . '/counter.tmp' );
// read counter
$counter = ( int )$fl->readAll();
// writing hit
$fl->rewrite( ++$counter );
echo "Current counter value: {$fl->readAll()}";
// close counter
$fl->close();