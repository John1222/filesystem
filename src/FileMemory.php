<?php
namespace Brocoder\FileSystem;

class FileMemory implements FileI
{
    /**
     * @var File
     */
    private $file;
    private $content;
    private $isLocked;

    /**
     * @param string $fileName
     * @param bool $lock
     * @throws FileOpeningFailedException
     */
    public function __construct( $fileName, $lock = false )
    {
        $this->file = new File( $fileName, $lock );
        $this->content = $this->file->readAll();
        $this->isLocked = $lock;
    }

    /**
     * @return string
     */
    public function getMemoryContent()
    {
        return $this->content;
    }

    /**
     * @return void
     */
    public function clear()
    {
        $this->content = '';
    }

    /**
     * @param string $string
     * @return void
     */
    public function rewrite( $string )
    {
        $this->content = $string;
    }

    /**
     * @param string $string
     * @return void
     */
    public function prepend( $string )
    {
        $this->content = $string . $this->content;
    }

    /**
     * @param string $string
     * @return void
     */
    public function append( $string )
    {
        $this->content .= $string;
    }

    /**
     * @param int $seek
     * @param int $length
     * @return string
     */
    public function read( $seek, $length )
    {
        return substr( $this->content, $seek, $length );
    }

    /**
     * @return string
     */
    public function readAll()
    {
        return $this->content;
    }

    /**
     * @return array
     */
    public function readAllAsLines()
    {
        return explode( $this->file->getEOL(), $this->content );
    }

    /**
     * @param int $lineNum
     * @return string
     */
    public function readLine( $lineNum )
    {
        return $this->readAllAsLines()[ $lineNum ];
    }

    /**
     * @param $lineNum
     * @return string
     */
    public function readAndRemoveLine( $lineNum )
    {
        $line = $this->readLine( $lineNum );
        $this->removeLine( $lineNum );
        return $line;
    }

    /**
     * @param int $lineNum
     * @return void
     */
    public function removeLine( $lineNum )
    {
        $lines = $this->readAllAsLines();
        unset( $lines[ $lineNum ] );
        $this->content = implode( $lines, $this->file->getEOL() );
    }

    /**
     * @param string $regex
     * @param string $replacement
     * @param int $limit
     * @return void
     */
    public function preg_replace( $regex, $replacement, $limit = -1 )
    {
        $this->content = preg_replace( $regex, $replacement, $this->content, $limit );
    }

    /**
     * @param string $destFileName
     * @return bool
     */
    public function copyTo( $destFileName )
    {
        try {
            $destFile = new File( $this->file->getFileName(), $this->isLocked );
            $isCopied = $destFile->copyTo( $destFileName );
            $isClosed = $destFile->close();
            return $isCopied && $isClosed;
        }
        catch( FileOpeningFailedException $e ) {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function save()
    {
        return $this->file->rewrite( $this->content );
    }

    /**
     * @return bool
     */
    public function close()
    {
        return $this->file->close();
    }

    /**
     * @return bool
     */
    public function saveAndClose()
    {
        $isSaved = $this->save();
        $isClosed = $this->close();
        return $isSaved && $isClosed;
    }
}