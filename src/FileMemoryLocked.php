<?php
namespace Brocoder\FileSystem;

class FileMemoryLocked extends FileMemory
{
    /**
     * @param string $fileName
     * @throws FileOpeningFailedException
     * @see \Brocoder\FileSystem\FileMemory
     */
    public function __construct( $fileName )
    {
        parent::__construct( $fileName, true );
    }
}