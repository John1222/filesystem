<?php
namespace Brocoder\FileSystem;

class SFile
{
    /**
     * @param $fileName
     * @param bool $lock
     * @return bool
     * @throws FileOpeningFailedException
     * @see File::clear()
     */
    public static function clear( $fileName, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $isCleared = $fl->clear();
        $isClosed = $fl->close();
        return $isCleared && $isClosed;
    }

    /**
     * @param string $fileName
     * @param string $content
     * @param bool $lock
     * @return bool
     * @throws FileOpeningFailedException
     * @see File::rewrite()
     */
    public static function rewrite( $fileName, $content, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $isRewrote = $fl->rewrite( $content );
        $isClosed = $fl->close();
        return $isRewrote && $isClosed;
    }

    /**
     * @param string $fileName
     * @param string $content
     * @param bool $lock
     * @return bool
     * @throws FileOpeningFailedException
     * @see File::prepend()
     */
    public static function prepend( $fileName, $content, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $isPrepend = $fl->prepend( $content );
        $isClosed = $fl->close();
        return $isPrepend && $isClosed;
    }

    /**
     * @param string $fileName
     * @param string $content
     * @param bool $lock
     * @return bool
     * @throws FileOpeningFailedException
     * @see File::append()
     */
    public static function append( $fileName, $content, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $isAppended = $fl->append( $content );
        $isClosed = $fl->close();
        return $isAppended && $isClosed;
    }

    /**
     * @param string $fileName
     * @param int $seek
     * @param int $length
     * @param bool $lock
     * @return bool|string
     * @throws FileOpeningFailedException
     * @see File::read()
     */
    public static function read( $fileName, $seek, $length, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $read = $fl->read( $seek, $length );
        $isClosed = $fl->close();
        return ( $read !== false && $isClosed ) ? $read : false;
    }

    /**
     * @param string $fileName
     * @param bool $lock
     * @return bool|string
     * @throws FileOpeningFailedException
     * @see File::readAll()
     */
    public static function readAll( $fileName, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $read = $fl->readAll();
        $isClosed = $fl->close();
        return ( $read !== false && $isClosed ) ? $read : false;
    }

    /**
     * @param string $fileName
     * @param bool $lock
     * @return bool|string
     * @throws FileOpeningFailedException
     * @see File::readAllAsLines()
     */
    public static function readAllAsLines( $fileName, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $lines = $fl->readAllAsLines();
        $isClosed = $fl->close();
        return ( $lines !== false && $isClosed ) ? $lines : false;
    }

    /**
     * @param string $fileName
     * @param int $lineNum
     * @param bool $lock
     * @return bool|string
     * @throws FileOpeningFailedException
     * @see File::readLine()
     */
    public static function readLine( $fileName, $lineNum, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $line = $fl->readLine( $lineNum );
        $isClosed = $fl->close();
        return ( $line !== false && $isClosed ) ? $line : false;
    }

    /**
     * @param string $fileName
     * @param string $lineNum
     * @param bool $lock
     * @return bool|string
     * @throws FileOpeningFailedException
     * @see File::readAndRemoveLine()
     */
    public static function readAndRemoveLine( $fileName, $lineNum, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $read = $fl->readAndRemoveLine( $lineNum );
        $isClosed = $fl->close();
        return ( $read !== false && $isClosed ) ? $read : false;
    }

    /**
     * @param string $fileName
     * @param string $lineNum
     * @param bool $lock
     * @return bool|string
     * @throws FileOpeningFailedException
     * @see File::removeLine()
     */
    public static function removeLine( $fileName, $lineNum, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $isRemoved = $fl->removeLine( $lineNum );
        $isClosed = $fl->close();
        return ( $isRemoved !== false && $isClosed ) ? $isRemoved : false;
    }

    /**
     * @param $sourceFileName
     * @param string $destFileName
     * @param bool $lock
     * @return bool
     * @throws FileOpeningFailedException
     * @see File::copyTo()
     */
    public static function copyTo( $sourceFileName, $destFileName, $lock = false )
    {
        $fl = new File( $sourceFileName, $lock );
        $isCopied = $fl->copyTo( $destFileName );
        $isClosed = $fl->close();
        return ( $isCopied !== false && $isClosed ) ? $isCopied : false;
    }

    /**
     * @param string $fileName
     * @param string $pattern
     * @param string $replacement
     * @param int $limit
     * @param bool $lock
     * @return bool
     * @throws FileOpeningFailedException
     * @see File::preg_replace()
     */
    public static function preg_replace( $fileName, $pattern, $replacement, $limit = -1, $lock = false )
    {
        $fl = new File( $fileName, $lock );
        $isReplaced = $fl->preg_replace( $pattern, $replacement, $limit );
        $isClosed = $fl->close();
        return $isReplaced && $isClosed;
    }
}