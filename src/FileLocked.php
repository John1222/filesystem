<?php
namespace Brocoder\FileSystem;

class FileLocked extends File
{
    /**
     * @param string $fileName
     * @throws FileOpeningFailedException
     * @see \Brocoder\FileSystem\File
     */
    public function __construct( $fileName )
    {
        parent::__construct( $fileName, true );
    }
}