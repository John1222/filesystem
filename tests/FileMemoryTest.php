<?php
namespace Brocoder\FileSystem\Tests;

use Brocoder\FileSystem\FileMemory;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../vendor/autoload.php';

class FileMemoryTest extends TestCase implements FileTestI
{
    private static $testFileName;
    private const TEST_CONTENT = "0\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n";
    private static $tempFileName;
    /**
     * @var FileMemory
     */
    private $fm;

    protected function setUp()
    {
        self::$testFileName = sys_get_temp_dir() . '/test_file.txt';
        self::$tempFileName = sys_get_temp_dir() . '/test_file_temp.txt';
        
        file_put_contents( self::$testFileName, self::TEST_CONTENT );
        file_put_contents( self::$tempFileName, '' );
        
        $this->fm = new FileMemory( self::$testFileName );
    }

    public function tearDown()
    {
        $this->fm->close();
        unlink( self::$testFileName );
        unlink( self::$tempFileName );
    }
    
    public function testLock()
    {
        $file = new FileMemory( self::$testFileName, true );
        $concurrentFp = fopen( self::$testFileName, 'a+' );
        $this->assertFalse( flock( $concurrentFp, LOCK_EX | LOCK_NB ), 'We abnormally got a lock' );
        $file->close();
        $this->assertTrue( flock( $concurrentFp, LOCK_EX | LOCK_NB ), 'Can\'t get a lock' );
        fclose( $concurrentFp );
    }
    
    public function testClear()
    {
        $this->fm->clear();
        $this->assertEmpty( $this->fm->getMemoryContent() );
    }

    public function testRewrite()
    {
        $assertedString = 'rewrote';
        $this->fm->rewrite( $assertedString );
        $this->assertEquals( $assertedString, $this->fm->getMemoryContent() );
    }

    public function testPrepend()
    {
        $assertedString = 'prepended';
        $this->fm->prepend( $assertedString );
        $this->assertTrue( ( boolean ) preg_match( "/^{$assertedString}/", $this->fm->getMemoryContent() ) );
    }

    public function testAppend()
    {
        $assertedString = 'appended';
        $this->fm->append( $assertedString );
        $this->assertTrue( ( boolean ) preg_match( "/{$assertedString}$/", $this->fm->getMemoryContent() ) );
    }

    public function testRead()
    {
        $seek = mt_rand( 0, strlen( self::TEST_CONTENT ) - 1 );
        $length = 3;
        $expected = substr( self::TEST_CONTENT, $seek, $length );
        $actual = $this->fm->read( $seek, $length );
        $this->assertEquals( $expected, $actual );
    }

    public function testReadAll()
    {
        $this->assertEquals( self::TEST_CONTENT, $this->fm->getMemoryContent() );
    }

    public function testReadAllAsLines()
    {
        $readAllAsLines = $this->fm->readAllAsLines();
        $this->assertNotEquals( false, $readAllAsLines );
        foreach( $this->getTestContentAsLines() as $index => $line ) {
            $this->assertEquals(
                trim( $line ),
                $readAllAsLines[ $index ],
                'Something wrong with readAllAsLines() function'
            );
        }
    }

    public function testReadLine()
    {
        $assertionLineIndex = 1;
        $readLine = $this->fm->readLine( $assertionLineIndex );
        $this->assertNotEquals( false, $readLine );
        $this->assertEquals( $this->getTestContentAsLines()[ $assertionLineIndex ], $readLine );
    }

    public function testReadAndRemoveLine()
    {
        $testedLineNum = mt_rand( 0, count( $this->getTestContentAsLines() ) - 1 );
        $this->assertEquals( $testedLineNum, $this->fm->readAndRemoveLine( $testedLineNum ) );
        $this->assertFalse( in_array( $testedLineNum, $this->fm->readAllAsLines(), true ) );
    }

    public function testRemoveLine()
    {
        $testedLineNum = mt_rand( 0, count( $this->getTestContentAsLines() ) - 1 );
        $this->fm->readAndRemoveLine( $testedLineNum );
        $this->assertFalse( in_array( $testedLineNum, $this->fm->readAllAsLines(), true ) );
    }

    public function testPregReplace()
    {
        $testedLineNum = mt_rand( 0, count( $this->getTestContentAsLines() ) - 1 );
        $this->fm->preg_replace( "/{$testedLineNum}\r\n/", '', 1 );
        $this->assertFalse( in_array( $testedLineNum, $this->fm->readAllAsLines(), true ) );
    }

    public function testCopyTo()
    {
        $this->fm->copyTo( self::$tempFileName );
        $this->assertEquals( self::TEST_CONTENT, file_get_contents( self::$tempFileName ) );
    }

    public function testClose()
    {
        $this->assertTrue( $this->fm->close() );
    }
    
    public function testSave()
    {
        $assertedString = 'saved content';
        $this->fm->rewrite( $assertedString );
        $this->fm->save();
        $this->assertEquals( $assertedString, file_get_contents( self::$testFileName ) );
    }

    /**
     * @return array
     */
    private function getTestContentAsLines()
    {
        return explode( "\r\n", trim( self::TEST_CONTENT ) );
    }
}