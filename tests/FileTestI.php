<?php
namespace Brocoder\FileSystem\Tests;

interface FileTestI
{
    public function testLock();
    public function testClear();
    public function testRewrite();
    public function testPrepend();
    public function testAppend();
    public function testRead();
    public function testReadAll();
    public function testReadAllAsLines();
    public function testReadLine();
    public function testReadAndRemoveLine();
    public function testRemoveLine();
    public function testPregReplace();
    public function testCopyTo();
    public function testClose();
}